ALPHA = 3  # Nombre de redécoupage de la séquence dans la fonction evaluate2
p = 0.2
Pm = 0.3
size = 64
nb_iter = 50
AGRANDISSEMENT = 5  # C'est le nombre de point qu'on utilise pour recoller
# facteur permettant d'équilibrer l'objectif de minimisation de distante et minimisation de l'angle
Prise_en_compte_angle = 5
nom_selection = "selection_tournoi"
nom_croisement = "croisement_en_un_point"
List_of_genes = ["AA", "AC", "AG", "AT",
                 "TA", "CA", "CC", "CG", "GC", "CT"]  # les gènes à modifier
Symetrique = {"AA": "TT", "AC": "TG", "AG": "TC", "AT": None,
              "TA": None, "CA": "GT", "CC": "GG", "CG": None, "GC": None, "CT": "GA"}  # les symétriques de ces gènes
pas = 10
