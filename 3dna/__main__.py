import matplotlib.pyplot as plt
from .RotTable import RotTable
from .Traj3D import Traj3D
from .Population import *
import argparse
import time
import math
from .Constantes import *
parser = argparse.ArgumentParser()

parser.add_argument("filename", help="input filename of DNA sequence")
parser.parse_args()
args = parser.parse_args()


def tests_croisements():
    D_un = []
    D_deux = []
    D_milieu = []
    N = [i for i in range(nb_iter)]
    rot_table = RotTable()
    traj = Traj3D()
    traj_final = Traj3D()
    # Read file
    lineList = [line.rstrip('\n') for line in open(args.filename)]
    # Formatting
    seq = ''.join(lineList[1:])
    popu1 = Population(size)
    popu2 = Population(size)
    popu2.troupeau = popu1.troupeau.copy()
    popu3 = Population(size)
    popu3.troupeau = popu1.troupeau.copy()
    for i in range(nb_iter):
        print(i)
        _, value = popu1.meilleur(seq)
        D_un.append(math.sqrt(value))
        popu1.iteration(seq, nom_selection, "croisement_en_un_point")
    for i in range(nb_iter):
        print(i)
        _, value = popu2.meilleur(seq)
        D_deux.append(math.sqrt(value))
        popu2.iteration(seq, nom_selection, "croisement_en_deux_points")
    for i in range(nb_iter):
        print(i)
        _, value = popu3.meilleur(seq)
        D_milieu.append(math.sqrt(value))
        popu3.iteration(seq, nom_selection, "croisement_milieu")
    plt.clf()
    plt.plot(N, D_un, label='un')
    plt.plot(N, D_deux, label='deux')
    plt.plot(N, D_milieu, label='milieu')
    plt.yscale('log')
    plt.xlabel("Iteration")
    plt.ylabel("Distance")
    plt.grid()
    plt.legend()
    plt.show()
    plt.savefig("tests_croisements.png")


def tests_selection():
    D_un = []
    D_deux = []
    D_milieu = []
    N = [i for i in range(nb_iter)]
    rot_table = RotTable()
    traj = Traj3D()
    traj_final = Traj3D()
    # Read file
    lineList = [line.rstrip('\n') for line in open(args.filename)]
    # Formatting
    seq = ''.join(lineList[1:])
    popu1 = Population(size)
    popu2 = Population(size)
    popu2.troupeau = popu1.troupeau.copy()
    popu3 = Population(size)
    popu3.troupeau = popu1.troupeau.copy()
    for i in range(nb_iter):
        print(i)
        _, value = popu1.meilleur(seq)
        D_un.append(math.sqrt(value))
        popu1.iteration(seq, "selection_roulette", nom_croisement)
    for i in range(nb_iter):
        print(i)
        _, value = popu2.meilleur(seq)
        D_deux.append(math.sqrt(value))
        popu2.iteration(seq, "selection_elitisme", nom_croisement)
    for i in range(nb_iter):
        print(i)
        _, value = popu3.meilleur(seq)
        D_milieu.append(math.sqrt(value))
        popu3.iteration(seq, "selection_tournoi", nom_croisement)
    plt.clf()
    plt.plot(N, D_un, label='selection_roulette')
    plt.plot(N, D_deux, label='selection_elitisme')
    plt.plot(N, D_milieu, label='selection_tournoi')
    plt.yscale('log')
    plt.xlabel("Iteration")
    plt.ylabel("Distance")
    plt.grid()
    plt.legend()
    plt.show()
    plt.savefig("selection_methodes.png")

def tests_selection_size():
    D_un = []
    D_deux = []
    D_milieu = []
    S = [5*i for i in range(1,21)]
    rot_table = RotTable()
    traj = Traj3D()
    traj_final = Traj3D()
    # Read file
    lineList = [line.rstrip('\n') for line in open(args.filename)]
    # Formatting
    seq = ''.join(lineList[1:])
    for size in S:
        popu1 = Population(size)
        popu2 = Population(size)
        popu2.troupeau = popu1.troupeau.copy()
        popu3 = Population(size)
        popu3.troupeau = popu2.troupeau.copy()
        popu1.make_evolution_iter(seq, 'selection_roulette', nom_croisement, nb_iter)
        popu2.make_evolution_iter(seq, 'selection_elitisme', nom_croisement, nb_iter)
        popu3.make_evolution_iter(seq, 'selection_tournoi', nom_croisement, nb_iter)
        _, value1 = popu1.meilleur_distance(seq)
        _, value2 = popu2.meilleur_distance(seq)
        _, value3 = popu3.meilleur_distance(seq)
        D_un.append(value1)
        D_deux.append(value2)
        D_milieu.append(value3)
    plt.clf()
    plt.plot(S, D_un, label='selection_roulette')
    plt.plot(S, D_deux, label='selection_elitisme')
    plt.plot(S, D_milieu, label='selection_tournoi')
    plt.yscale('log')
    plt.xlabel("Size")
    plt.ylabel("Distance")
    plt.grid()
    plt.legend()
    plt.show()
    plt.savefig("selection_methodes_size.png")

def tests_croisements_size():
    D_un = []
    D_deux = []
    D_milieu = []
    nb_iter = 10
    S = [10*i for i in range(1,21)]
    rot_table = RotTable()
    traj = Traj3D()
    traj_final = Traj3D()
    # Read file
    lineList = [line.rstrip('\n') for line in open(args.filename)]
    # Formatting
    seq = ''.join(lineList[1:])
    for size in S:
        popu1 = Population(size)
        popu2 = Population(size)
        popu2.troupeau = popu1.troupeau.copy()
        popu3 = Population(size)
        popu3.troupeau = popu2.troupeau.copy()
        popu1.make_evolution_iter(seq, nom_selection, 'croisement_en_un_point', nb_iter)
        popu2.make_evolution_iter(seq, nom_selection, 'croisement_en_deux_points', nb_iter)
        popu3.make_evolution_iter(seq, nom_selection, 'croisement_milieu', nb_iter)
        _, _, value1 = popu1.meilleur_distance_angle(seq)
        _, _, value2 = popu2.meilleur_distance_angle(seq)
        _, _, value3 = popu3.meilleur_distance_angle(seq)
        D_un.append(value1)
        D_deux.append(value2)
        D_milieu.append(value3)
    plt.clf()
    plt.plot(S, D_un, label='croisement_en_un_point')
    plt.plot(S, D_deux, label='croisement_en_deux_points')
    plt.plot(S, D_milieu, label='croisement_milieu')
    plt.yscale('log')
    plt.xlabel("Size")
    plt.ylabel("Distance")
    plt.grid()
    plt.legend()
    plt.show()
    plt.savefig("croisement_methodes_size.png")

def test_opt():
    time0 = time.time()

    rot_table = RotTable()
    traj = Traj3D()
    traj_final = Traj3D()
    # Read file
    lineList = [line.rstrip('\n') for line in open(args.filename)]
    # Formatting
    seq = ''.join(lineList[1:])
    popu = Population(size)

    popu.make_evolution_opt(seq, nom_selection, nom_croisement, eps=1)
    final_tab, value = popu.meilleur(seq)
    final_tab.exportation()
    print(math.sqrt(value), "(distance optimisée)")
    rot_init = RotTable(randomize=False)
    print(math.sqrt(rot_init.evaluate(seq)), "(distance de base)")

    traj.compute(seq, rot_table)
    traj_final.compute(seq, final_tab)

    traj.draw(str(math.sqrt(rot_table.evaluate(seq))))
    traj.write(args.filename+".png")
    traj_final.draw(str(int(math.sqrt(final_tab.evaluate(seq)))))
    traj_final.write("data/traj_final.png")

    print("time :", time.time()-time0)


def generation():
    time0 = time.time()
    gen = [i for i in range(1, nb_iter+1)]
    moy = []
    meilleur = []
    rot_table = RotTable()
    traj_final = Traj3D()

    # Read file
    lineList = [line.rstrip('\n') for line in open(args.filename)]
    # Formatting
    seq = ''.join(lineList[1:])
    popu = Population(size)

    for j in range(nb_iter):

        popu.iteration(seq, nom_selection, nom_croisement)

        final_tab, value = popu.meilleur(seq)
        moy.append(popu.evaluate_moyenne(seq))
        meilleur.append(value)

    print(final_tab.distance(seq), "(distance optimisée)")
    print(rot_table.distance(seq), "(distance de base)")

    traj_final.compute(seq, final_tab)
    # traj_final.draw(str(int(final_tab.distance(seq))))
    # traj_final.write("data/traj_final.png")
    traj_final.draw_bouts(str(int(final_tab.distance(seq))))
    traj_final.write("data/traj_final_bouts.png")

    print("time :", time.time()-time0)
    plt.figure(5)
    plt.plot(gen, moy, label='score moyenne')
    plt.plot(gen, meilleur, label="score meilleur")
    plt.xlabel('génération')
    plt.ylabel('score (log)')
    plt.yscale('log')
    plt.grid()
    plt.legend()
    plt.savefig("data/évolution_graph_size_"+str(size)+"nbiter_"+str(nb_iter)+"_croisement_" +
                str(nom_croisement)+"_selection_"+str(nom_selection)+"AGRANDISSEMENT"+str(AGRANDISSEMENT)+".png")


def main():

    time0 = time.time()

    rot_table = RotTable()
    traj = Traj3D()
    traj_final = Traj3D()
    traj_final2 = Traj3D()
    # Read file
    lineList = [line.rstrip('\n') for line in open(args.filename)]
    # Formatting
    seq = ''.join(lineList[1:])
    popu = Population(size)

    popu.make_evolution_iter(seq, nom_selection, nom_croisement, nb_iter)

    final_tab, value = popu.meilleur(seq)
    print(final_tab.distance(seq), "(distance optimisée)")
    print(final_tab.angle(seq), "(angle optimisée)")
    rot_init = RotTable(randomize=False)
    print(rot_init.distance(seq), "(distance de base)")

    n = len(seq)//2
    seq2 = seq[n:]+seq[:n]

    traj.compute(seq, rot_table)
    traj_final.compute(seq, final_tab)
    traj_final2.compute(seq2, final_tab)
    traj_final.draw_bouts(str(int(final_tab.distance(seq))))
    traj_final.write("data/traj_final_bouts.png")

    traj.draw(str(rot_table.distance(seq)))
    traj.write(args.filename+".png")
    traj_final.draw(str(int(final_tab.distance(seq))))
    traj_final.write("data/traj_final.png")
    traj_final2.draw(str(int(final_tab.distance(seq2))))
    traj_final2.write("data/traj_final2.png")
    print("time :", time.time()-time0)

def evolution_fitness():#evolution de la fitness au cours de l'évaluation
    D_dist = []
    D_score = []
    N = [i for i in range(nb_iter)]
    rot_table = RotTable()
    traj = Traj3D()
    traj_final = Traj3D()
    # Read file
    lineList = [line.rstrip('\n') for line in open(args.filename)]
    # Formatting
    seq = ''.join(lineList[1:])
    popu1 = Population(size)
    for i in range(nb_iter):
        print(i)
        _, value = popu1.meilleur_distance(seq)
        D_dist.append(value)
        _, value = popu1.meilleur(seq)
        D_score.append(value/1e4)
        popu1.iteration(seq, nom_selection, nom_croisement)
    plt.clf()
    plt.plot(N, D_dist, label='Distance')
    plt.plot(N, D_score, label='Score')
    plt.yscale('log')
    plt.xlabel("Iteration")
    plt.ylabel("Distance/Score")
    plt.grid()
    plt.legend()
    plt.show()
    plt.savefig("fitness au cours de l'evo.png")
    #plt.close()

def score_angle_dist():#tracé de l'angle au cours de la minimisation
    D_dist = []
    D_score = []
    L_angle = []
    N = [i for i in range(nb_iter)]
    rot_table = RotTable()
    traj = Traj3D()
    traj_final = Traj3D()
    # Read file
    lineList = [line.rstrip('\n') for line in open(args.filename)]
    # Formatting
    seq = ''.join(lineList[1:])
    popu1 = Population(size)
    for i in range(nb_iter):
        print(i)
        _, value, angle = popu1.meilleur_distance_angle(seq)
        D_dist.append(value)
        L_angle.append(angle)
        _, value = popu1.meilleur(seq)
        D_score.append(value/1e4)
        popu1.iteration(seq, nom_selection, nom_croisement)
    plt.clf()
    plt.subplot(1,2,1)
    plt.plot(N, D_dist, label='Distance')
    plt.plot(N, D_score, label='Score')
    plt.xlabel("Iteration")
    plt.ylabel("Distance/Score")
    plt.yscale('log')
    plt.grid()
    plt.legend()
    plt.subplot(1,2,2)
    plt.plot(N,L_angle)
    plt.xlabel("Iteration")
    plt.ylabel("Angle")
    plt.grid()  
    plt.show()
    plt.savefig("fitness au cours de l'evo.png")
    #plt.close()

def fitness_size():  # evolution de la fitness au cours de l'évaluation
    D = []
    S = [int(1.5**i+10) for i in range(10)]

    time0 = time.time()

    rot_table = RotTable()
    traj = Traj3D()
    traj_final = Traj3D()
    # Read file
    lineList = [line.rstrip('\n') for line in open(args.filename)]
    # Formatting
    seq = ''.join(lineList[1:])
    for size in S:
        popu = Population(size)
        popu.make_evolution_iter(seq, nom_selection, nom_croisement, nb_iter)
        final_tab, value = popu.meilleur_distance(seq)
        final_tab.exportation()
        print(value, "(distance optimisée)")
        rot_init = RotTable(randomize=False)
        print(rot_init.distance(seq), "(distance de base)")
        D.append(value)
    plt.clf()
    plt.plot(S, D)
    plt.yscale('log')
    plt.xlabel("Taille de la population")
    plt.ylabel("Distance")
    plt.grid()
    plt.legend()
    plt.show()
    plt.savefig("distance et size.png")
    # plt.close()


if __name__ == "__main__":
    main()
    #tests_selection_size()
    #tests_croisements_size()
    '''
    fitness_size()
    main()
    test_opt()
    tests_croisements()
    tests_selection()
    evolution_fitness()
    tests_selection_size()
    tests_croisements_size()
    score_angle_dist()
    '''
    
 