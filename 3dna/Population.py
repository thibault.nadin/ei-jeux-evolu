from math import inf
from .Croisement import *
import random
from .RotTable import RotTable
from .Selections import *
from .Constantes import *


class Population:

    def __init__(self, size: int):
        self.troupeau = [RotTable(randomize=True) for k in range(size)]
        self.size = size

    def selection(self, nom_selection, molecule):
        self.troupeau = Dico_selections[nom_selection](molecule, self.troupeau)
        self.size = len(self.troupeau)

    def croisement(self, nom_croisement):
        n = self.size
        trp = self.troupeau
        nuls = trp[1:]
        rd.shuffle(nuls)
        self.troupeau = [trp[0]]+nuls
        # le meilleur reste à la première place pour éviter de le faire muter ensuite
        for k in range(n//2):
            self.troupeau = self.troupeau + \
                list(Dico_Croisements[nom_croisement](
                    self.troupeau[2*k], self.troupeau[2*k+1]))
        self.size = len(self.troupeau)

    def mutations(self, molecule):
        for k in range(1, self.size):
            if random.random() < p:
                self.troupeau[k].mutate(molecule)

    def iteration(self, molecule, nom_selection, nom_croisement):
        self.selection(nom_selection, molecule)
        self.croisement(nom_croisement)
        self.mutations(molecule)

    def meilleur(self, molecule):
        score = inf
        for x in self.troupeau:
            scoreprovisoire = x.evaluate(molecule)
            if scoreprovisoire < score:
                score = scoreprovisoire
                m = x
        return m, score

    def meilleur_distance(self, molecule):
        score = inf
        for x in self.troupeau:
            scoreprovisoire = x.distance(molecule)
            if scoreprovisoire < score:
                score = scoreprovisoire
                m = x
        return m, score
    
    def meilleur_distance_angle(self, molecule):
        m, _ = self.meilleur(molecule)
        return m, m.distance(molecule), m.angle(molecule)

    def evaluate_moyenne(self, molecule):
        score = 0
        for x in self.troupeau:
            score += x.evaluate(molecule)
        return score/self.size

    def make_evolution_opt(self, molecule, nom_selection, nom_croisement, eps=1):
        _, fx = self.meilleur(molecule)
        fy = fx+2*eps
        while abs(fy - fx) > eps:
            self.iteration(molecule, nom_selection, nom_croisement)
            fx = fy
            _, fy = self.meilleur(molecule)

    def make_evolution_iter(self, molecule, nom_selection, nom_croisement, nb_iter):
        for i in range(nb_iter):
            self.iteration(molecule, nom_selection, nom_croisement)
            # print(str(i)+"iterations")
