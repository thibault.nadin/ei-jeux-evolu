from .RotTable import RotTable
from .Croisement import *
from .Selections import *
from .Traj3D import Traj3D
from .Population import *
from copy import deepcopy
##Tests Croisement

initial_rot_table = json_load(open(join(os_path.abspath(os_path.dirname(__file__)), 'table.json')))

for _ in range(10):
    for gene in initial_rot_table.keys():
        p1 = RotTable(randomize = True)
        p2 = RotTable(randomize = True)
        p_fils = croisement_en_un_point(p1, p2)[0]
        rot_croisement1 = p_fils.getTable()
        assert isinstance(p_fils, RotTable)
        assert p_fils.getTable() != p1.getTable() and p_fils.getTable() != p2.getTable()
        assert rot_croisement1.keys() == p1.getTable().keys()
        assert p_fils.getTwist(gene)>= initial_rot_table[0] - initial_rot_table[3]  and p_fils.getTwist(gene) <= initial_rot_table[0] + initial_rot_table[3] 
        assert p_fils.getWedge(gene) >= initial_rot_table[1] - initial_rot_table[4] and p_fils.getWedge(gene) <= initial_rot_table[1] + initial_rot_table[4]
        assert p_fils.getDirection(gene) >= initial_rot_table[2] - initial_rot_table[5] and p_fils.getDirection(gene) <= initial_rot_table[2] + initial_rot_table[5]

for _ in range(10):
    for gene in initial_rot_table.keys():
        p1 = RotTable(randomize = True)
        p2 = RotTable(randomize = True)
        p_fils = croisement_en_deux_points(p1, p2)[0]
        rot_croisement1 = p_fils.getTable()
        assert isinstance(p_fils, RotTable)
        assert p_fils.getTable() != p1.getTable() and p_fils.getTable() != p2.getTable()
        assert rot_croisement1.keys() == p1.getTable().keys()
        assert p_fils.getTwist(gene)>= initial_rot_table[0] - initial_rot_table[3]  and p_fils.getTwist(gene) <= initial_rot_table[0] + initial_rot_table[3] 
        assert p_fils.getWedge(gene) >= initial_rot_table[1] - initial_rot_table[4] and p_fils.getWedge(gene) <= initial_rot_table[1] + initial_rot_table[4]
        assert p_fils.getDirection(gene) >= initial_rot_table[2] - initial_rot_table[5] and p_fils.getDirection(gene) <= initial_rot_table[2] + initial_rot_table[5]
    
##Test population :
#test iteration : on vérifie si l'iteration n'augmente pas le score du meilleur individu
for _ in range(10):
    initial_pop = Population(10)
    next_pop = Population(10)
    next_pop.troupeau = deepcopy(initial_pop.troupeau)
    next_pop.iteration(seq, 'selection_roulette', 'croisement_en_deux_point')
    _, initial_value = initial_pop.meilleur(seq)
    _, next_value = next_pop.meilleur(seq)
    assert isinstance(next_pop, Population)
    assert initial_value>=next_value

##Test mutation :
#On vérifie que la mutation retourne bien une RotTable avec les valeurs dans les intervalles souhaités
for _ in range(100):
    person = RotTable(randomize = True)
    person.mutate()
    assert isinstance(person, RotTable)
    assert person.getTwist(gene)>= initial_rot_table[0] - initial_rot_table[3]  and person.getTwist(gene) <= initial_rot_table[0] + initial_rot_table[3] 
    assert person.getWedge(gene) >= initial_rot_table[1] - initial_rot_table[4] and person.getWedge(gene) <= initial_rot_table[1] + initial_rot_table[4]
    assert person.getDirection(gene) >= initial_rot_table[2] - initial_rot_table[5] and person.getDirection(gene) <= initial_rot_table[2] + initial_rot_table[5]

##Test selection :
#selection_elitisme(molecule, liste_individus)
pop = Population()
individus = pop.troupeau
individus_values = [ind.evaluate() for ind in individus]
expected = [e for _,e in sorted(zip(individus_values,individus))][:len(individus)//2]
assert selection_elitisme(seq, individus) == expected

#selection_tournoi(molecule, liste_individus)
assert len(selection_tournoi(seq, individus)) == len(individus)//2
assert sum(expected) >= sum(selection_tournoi(seq, individus))

#selection_roulette(molecule, liste_individus)
assert len(selection_roulette(seq, individus)) == len(individus)//2
assert sum(expected) >= sum(selection_roulette(seq, individus))