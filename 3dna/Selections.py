import random
from math import inf


def selection_elitisme(molecule, liste_individus):
    p = len(liste_individus)//2
    liste_individus.sort(key=lambda x: x.evaluate(molecule))
    return liste_individus[:p]


# Fonction qui prend en entrée une liste de score d'individus et renvoie la liste des scores des individus par une sélection par tournoi
def selection_tournoi(molecule, liste_individus):
    l = len(liste_individus)
    liste_token = [True]*len(liste_individus)
    p = l//2
    score = inf
    i = 0
    liste_score = []
    for x in liste_individus:
        scoreprovisoire = x.evaluate(molecule)
        liste_score.append(scoreprovisoire)
        if scoreprovisoire < score:
            score = scoreprovisoire
            meilleur = x
            k = i
        i += 1
    # On ajoute le meilleur individu (on le garde forcément)
    nouvelle_liste = [meilleur]
    liste_token[k] = False
    # Début du tournoi
    indices_attente = [i for i in range(l) if liste_token[i]]
    while len(nouvelle_liste) <= p and len(indices_attente) >= 4:
        # On choisit deux individus au hasard et on les supprime de la liste d'attente
        indices_attente = [i for i in range(l) if liste_token[i]]

        ind1, ind2 = random.sample(indices_attente, 2)
        liste_token[ind1], liste_token[ind2] = False, False
        if liste_score[ind1] < liste_score[ind2]:
            nouvelle_liste.append(liste_individus[ind1])
        else:
            nouvelle_liste.append(liste_individus[ind2])
    return nouvelle_liste


# n est le nombre de fois où on lancera la roulette
def selection_roulette(molecule, liste_individus, n=None):
    l = len(liste_individus)
    p = l//2
    if n == None:
        n = 2*len(liste_individus)

    liste = [(individu, individu.evaluate(molecule))
             for individu in liste_individus[1:]]

    s = 0
    new = [0]
    for nb in range(0, len(liste)):
        s = liste[nb][1]+s
        new.append(s)

    min = new[0]
    max = new[-1]
    liste_pires = [[0, liste[i][0]] for i in range(0, len(liste))]

    for exp in range(0, n):  # On lance la roulette n fois
        aleat = random.uniform(min, max)  # On choisit un nombre aléatoire
        for i in range(0, len(new)):
            if new[i] < aleat and aleat < new[i+1]:
                # Si il tombe dans l'individu i, on l'ajoute à la liste des pires
                liste_pires[i][0] = liste_pires[i][0]+1
        liste_pires.sort(key=lambda x: x[0])
    return [liste_individus[0]]+[liste_pires[i][1] for i in range(p-1)]

    # On élimine les individus sur lesquels on est tombé plusieurs fois (car on veut éliminer les distances)


def roulette(molecule, liste_individus):
    val_ind = [ind.evaluate(molecule) for ind in liste_individus]
    values_alea = [val * random.random() for val in val_ind]
    values_alea[0] = 0 #On garde le meilleur surement
    liste_individus = [e for _, e in sorted(zip(values_alea, liste_individus))]
    return liste_individus[:len(liste_individus)//2]


Dico_selections = {"selection_roulette": roulette,
                   "selection_elitisme": selection_elitisme,
                   "selection_tournoi": selection_tournoi
                   }
